//
//  ViewController.swift
//  USDtoYen
//
//  Created by Steve Fitzsimmons on 4/8/19.
//  Copyright © 2019 Steve Fitzsimmons. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var Text: UITextField!
    @IBOutlet weak var Label: UILabel!
    //@IBAction func Convert(_ sender: Any) {
   // }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Convert(_ sender: Any)
    {
        //Get the input from the text field
        
        let TextFieldValue = Double(Text.text!)
        
        //if statement to make sure user cannot leave this Text Field blank
        if TextFieldValue != nil
        {
            let result = Double (TextFieldValue! * 112.57)
            
            Label.text = "$\(TextFieldValue!) = ¥\(result)"
            //Clear text field after clicking the button
            Text.text = ""
        }
        else
        {
            Label.text = "This field cannot be blank!"
        }
    }


}

