//
//  ViewController.swift
//  USDtoYen
//
//  Created by Steve Fitzsimmons on 4/8/19.
//  Copyright © 2019 Steve Fitzsimmons. All rights reserved.
//

import UIKit
import Foundation

struct currencyRates: Decodable
{
    let base: String
    let date: String
    let time_last_updated: Int
    let rates: Dictionary<String, Double>
}

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    var pickerData = ["AUD", "CAD", "CHF", "CNY", "EUR", "GBP", "HKD", "ILS", "INR", "JPY", "KRW", "MXN", "MYR", "NOK", "NZD", "RUB", "SEK", "SGD", "THB", "TRY", "USD", "ZAR"]
    
    var rate = 0.0;
    var newCurrency = ""
    
    @IBOutlet weak var pickerView1: UIPickerView!
    @IBOutlet weak var Text: UITextField!
    @IBOutlet weak var Label: UILabel!
    //@IBAction func Convert(_ sender: Any) {
   // }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        pickerView1.showsSelectionIndicator = true
        
        self.pickerView1.delegate = self as? UIPickerViewDelegate
        self.pickerView1.dataSource = self as? UIPickerViewDataSource
    }

    @IBAction func convertButton(_ Sender: Any)
    {
        let inputValue = Double(inputUSD.text!)
        
        if inputValue != nil
        {
            let result = Double(inputValue! * rate)
            outPutYEN.text = String(result)
            
            inputUSD.text = ""
        }
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //number of items.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    //names of each item
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    //Preconditions: All storyboard objects and variables initilized.
    //Postconditions: After the user has selected a item the Async call is made and the currency rates are downloaded from the server. The corresponding rate is set to be calculated once the convert button is pressed.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let urlLink = "https://api.exchangerate-api.com/v4/latest/USD"
        let url = URL(string: urlLink)
        
        
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            
            //do,catch loop is required to avoid error and crash.
            do {
                //download data
                let ratesData = try JSONDecoder().decode(currencyRates.self, from: data!)
                print(ratesData.rates[self.newCurrency]!)
                self.rate = ratesData.rates[self.newCurrency]!
                
                
            } catch {
                print("Error")
            }
            
            
            
            }.resume()
        
        //Always use switch apposed to if else for faster execution
        switch (pickerData[row]) {
        case "ZAR":
            newCurrency = "ZAR"
            break;
        case "USD":
            newCurrency = "USD"
            
            break;
        case "TRY":
            newCurrency = "TRY"
            
            break;
        case "THB":
            newCurrency = "THB"
            
            break;
        case "SGD":
            newCurrency = "SGD"
            
            break;
        case "SEK":
            newCurrency = "SEK"
            
            break;
        case "RUB":
            newCurrency = "RUB"
            
            break;
        case "NZD":
            newCurrency = "NZD"
            
            break;
        case "NOK":
            newCurrency = "NOK"
            
            break;
        case "MYR":
            newCurrency = "MYR"
            
            break;
        case "MXN":
            newCurrency = "MXN"
            
            break;
        case "KRW":
            newCurrency = "KRW"
            
            break;
        case "JPY":
            newCurrency = "JPY"
            
            break;
        case "INR":
            newCurrency = "INR"
            
            break;
        case "ILS":
            newCurrency = "ILS"
            
            break;
        case "HKD":
            newCurrency = "HKD"
            
            break;
        case "GBP":
            newCurrency = "GBP"
            
            break;
        case "EUR":
            newCurrency = "EUR"
            
            break;
        case "CNY":
            newCurrency = "CNY"
            
            break;
        case "CHF":
            newCurrency = "CHF"
            
            break;
        case "CAD":
            newCurrency = "CAD"
            
            break;
        case "AUD":
            newCurrency = "AUD"
            
            break;
        default:
            newCurrency = "USD"
            break;
        }
        self.view.endEditing(true)
        pickerView1.isHidden = true;
    }//END OF picker function
    
    //END PICKER VIEW SUPPORTING FUNCTIONS
    
    
}//END OF CONTROLLER

    
    /*override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Convert(_ sender: Any)
    {
        //Get the input from the text field
        
        let TextFieldValue = Double(Text.text!)
        
        //if statement to make sure user cannot leave this Text Field blank
        if TextFieldValue != nil
        {
            let result = Double (TextFieldValue! * 112.57)
            
            Label.text = "$\(TextFieldValue!) = ¥\(result)"
            //Clear text field after clicking the button
            Text.text = ""
        }
        else
        {
            Label.text = "This field cannot be blank!"
        }
    }
*/

}

